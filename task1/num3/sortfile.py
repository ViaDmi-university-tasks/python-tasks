#!/usr/bin/env python
import argparse

def sort_file(file_name):
    """
    Sort lines in file.
    """
    with open(file_name, "r") as file_object:
        lines = file_object.readlines()
    if(not '\n' in lines[-1]):
        lines[-1] += '\n'
    sorted_lines = sorted(lines)
    with open('sorted' + file_name, "w") as file_object:
        file_object.writelines(sorted_lines)
    

def main(args):
    sort_file(args.file_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_name', action='store', help='input file name')
    args = parser.parse_args()
    main(args)
