#!/usr/bin/env python
import argparse
import sys


class Rectangle(object):
    def __init__(self, width, height):
        if(not isinstance(width, int)):
            sys.exit("Error in Rectangle: width is not integer!")
        if(not isinstance(height, int)):
            sys.exit("Error in Rectangle: height is not integer!")
        self.width = width
        self.height = height

    def area(self):
        return(self.width * self.height)

    def perimeter(self):
        return(2 * (self.width + self.height))

    def draw(self, filled):
        """
        Draw rectangle in terminal (use '#'). Fill it if filled is true.
        """
        for line in range(self.height):
            if(filled or line == 0 or line == self.height - 1):
                print(self.width * '#')
            else:
                print('#' + (self.width - 2) * ' ' + '#')


def main(args):
    rectan = Rectangle(args.width, args.height)
    print(f"Rectangle area = {rectan.area()}")
    print(f"Rectangle perimeter = {rectan.perimeter()}")
    print("Unfilled rectangle:")
    rectan.draw(False)
    print("Filled rectangle:")
    rectan.draw(True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('width',  action='store', type=int, help='input value')
    parser.add_argument('height', action='store', type=int, help='input value')
    args = parser.parse_args()
    main(args)
