#!/usr/bin/env python
import argparse
import sys

wallis_product_array = [1.0]


def get_wallis_product(n):
    """
    Return wallis product of first n factors.
    """
    if(n < 1 or not isinstance(n, int)):
        sys.exit("Incorrect argument in get_wallis_product (n < 1 or not integer).")
    m = len(wallis_product_array)
    if(n >= m):
        wallis_product = wallis_product_array[m-1]
        numerator = 2 * ((m + 1) // 2)
        denominator = 2 * (m // 2) + 1
        for i in range(m, n + 1):
            wallis_product = wallis_product_array[i-1]
            wallis_product *= numerator / denominator
            wallis_product_array.append(wallis_product)
            if (i % 2 == 0):
                numerator += 2
            else:
                denominator += 2
        return(wallis_product)
    else:
        return(wallis_product_array[n])


def print_wallis_table(file_name, number_list):
    """
    Print formated table of wallis products (number - wallis product) in file.
    """
    with open(file_name, 'w') as table_file:
        table_file.write("n".ljust(16) + ' ' + 'Pi' + '\n')
        for number in number_list:
            line = str(number).ljust(16) + ' ' + str(2 * get_wallis_product(number))
            table_file.write(line + '\n')


def main(args):
    number_list = [1, 5, 10, 50, 100, 1000, 10000]
    number_list += [100000]#, 10000000, 100000000]
    print_wallis_table(args.file_name, number_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_name', action='store', help='input file name')
    args = parser.parse_args()
    main(args)
