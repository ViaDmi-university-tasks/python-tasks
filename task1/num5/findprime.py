#!/usr/bin/env python
import argparse
import sys
from math import sqrt


def find_prime_number(n):
    """
    Return n's prime number.
    """
    if(n < 1 or not isinstance(n, int)):
        sys.exit("Incorrect argument in find_prime_number (n < 1 or not integer).")
    number = 3
    primes = [2]
    iteration = 1
    while iteration < sqrt(n) + 1:
        for divider in primes:
            if(number % divider == 0):
                break
        else:
            primes.append(number)
            iteration += 1
        number += 1
    return(primes[-1])


def main(args):
    print(f"N's prime: {find_prime_number(args.number)}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('number', action='store', type=int, help='input integer number')
    args = parser.parse_args()
    main(args)
