#!/usr/bin/env python
import argparse
import collections


def remove_punc_marks(text):
    """
    Remove punctuation marks in the text.
    """
    marks = ['.', ',', '?', '!', ';', ':', '"', '"']
    for mark in marks:
        text = text.replace(mark, '')
    return(text)


def find_common_words(file_name, n):
    """
    Find n the most common words in file.
    """
    f_dict = collections.defaultdict(lambda : 0)
    with open(file_name, "r") as file_object:
        for line in file_object:
            line = line.strip()
            line = remove_punc_marks(line)
            line = line.split()
            for word in line:
                word = word.lower()
                f_dict[word] += 1
    words = sorted(f_dict, key=f_dict.get, reverse=True)
    return(words[:n])


def main(args):
    words = find_common_words(args.file_name, 5)
    print(words)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_name', action='store', help='input file name')
    args = parser.parse_args()
    main(args)
