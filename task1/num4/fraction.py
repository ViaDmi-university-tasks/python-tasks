#!/usr/bin/env python
import argparse
import sys

def get_gcd(a, b):
    """
    Return greatest common divisor of a and b.
    """
    if (b == 0):
        return(a)
    return get_gcd(b, a % b)

class Fraction(object):
    def __init__(self, numerator, denominator):
        if(not isinstance(numerator, int)):
            sys.exit("Error in Fraction: numerator is not integer!")
        if(not isinstance(denominator, int)):
            sys.exit("Error in Fraction: denominator is not integer!")
        if(denominator == 0):
            sys.exit("Error in Fraction constructor: denominator is zero!")
        gcd = get_gcd(numerator, denominator)
        self.numerator = numerator // gcd
        self.denominator = denominator // gcd

    def show(self):
        print(f"{self.numerator}/{self.denominator}")

    def __add__(self, other):
        a = self.numerator * other.denominator + other.numerator * self.denominator
        b = self.denominator * other.denominator
        return(Fraction(a, b))

    def __sub__(self, other):
        a = self.numerator * other.denominator - other.numerator * self.denominator
        b = self.denominator * other.denominator
        return(Fraction(a, b))

    def __mul__(self, other):
        a = self.numerator * other.numerator
        b = self.denominator * other.denominator
        return(Fraction(a, b))

    def __truediv__(self, other):
        a = self.numerator * other.denominator
        b = self.denominator * other.numerator
        return(Fraction(a, b))

    def __neg__(self):
        return(Fraction(-self.numerator,self.denominator))

    def __abs__(self):
        return(Fraction(abs(self.numerator),self.denominator))

    def __eq__(self, other):
        if(self.numerator == other.numerator and self.denominator == other.denominator):
            return(True)
        else:
            return(False)

    def __ne__(self, other):
        if(self.numerator == other.numerator and self.denominator == other.denominator):
            return(False)
        else:
            return(True)

    def __gt__(self, other):
        numerator = self.numerator * other.denominator - other.numerator * self.denominator
        if(numerator > 0):
            return(True)
        else:
            return(False)

    def __ge__(self, other):
        numerator = self.numerator * other.denominator - other.numerator * self.denominator
        if(numerator >= 0):
            return(True)
        else:
            return(False)

    def __lt__(self, other):
        numerator = self.numerator * other.denominator - other.numerator * self.denominator
        if(numerator < 0):
            return(True)
        else:
            return(False)

    def __le__(self, other):
        numerator = self.numerator * other.denominator - other.numerator * self.denominator
        if(numerator <= 0):
            return(True)
        else:
            return(False)


def main(args):
    frac1 = Fraction(args.a_numerator, args.a_denominator)
    frac2 = Fraction(args.b_numerator, args.b_denominator)

    print("Fractions:")
    frac1.show()
    frac2.show()

    print("Sum:")
    frac3 = frac1 + frac2
    frac3.show()

    print("Diff:")
    frac3 = frac1 - frac2
    frac3.show()

    print("Mul:")
    frac3 = frac1 * frac2
    frac3.show()

    print("Div:")
    frac3 = frac1 / frac2
    frac3.show()

    print("Neg:")
    frac3 = -frac1
    frac3.show()

    print("Abs:")
    frac3 = abs(frac1)
    frac3.show()

    print("Eq:")
    print(frac1 == frac2)

    print("Ne:")
    print(frac1 != frac2)

    print("gt:")
    print(frac1 > frac2)

    print("ge:")
    print(frac1 >= frac2)

    print("lt:")
    print(frac1 < frac2)

    print("le:")
    print(frac1 <= frac2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('a_numerator',   action='store', type=int, help='input value')
    parser.add_argument('a_denominator', action='store', type=int, help='input value')
    parser.add_argument('b_numerator',   action='store', type=int, help='input value')
    parser.add_argument('b_denominator', action='store', type=int, help='input value')
    args = parser.parse_args()
    main(args)
