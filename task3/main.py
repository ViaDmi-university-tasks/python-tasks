#!/usr/bin/python
import os
import sys
import argparse
import numpy as np
from scipy import optimize
from astropy.io import fits
from matplotlib import pyplot as plt


def model_fun(argument, amplitude, center, scale):
    """
    Model function for flux of slice of galaxy.
    """
    return amplitude / np.cosh((argument - center) / scale)**2


def fit_slice(index_data, slice_data, fun):
    """
    Fits model function on slice.
    """
    init_params = (np.max(slice_data), len(index_data)/2, 10.)
    bounds=((0., 0., 0), (np.max(slice_data), len(index_data), np.inf))
    # maxiter=5000+
    beta, sd_beta = optimize.curve_fit(fun, index_data, slice_data)#, p0=init_params, bounds=bounds)
    return np.abs(beta), sd_beta # abs for (-scales <-> scales)


def get_slice_parameters(data):
    """
    Returns parameters (amplitudes, centers, scales) of fits data.
    """
    data -= np.min(data)
    fun = lambda x, b1, b2, b3: model_fun(x, b1, b2, b3)
    # functools determinate

    amplitudes = {'est': [], 'stdev': []}
    centers = {'est': [], 'stdev': []}
    scales = {'est': [], 'stdev': []}
    x_data = np.arange(len(data[0,:]))

    for i in range(len(data[:,0])):
        slice_data = data[i,:]
        params, params_std = fit_slice(x_data, slice_data, fun)

        amplitudes['est'].append(params[0])
        amplitudes['stdev'].append(np.sqrt(params_std[0][0]))
        centers['est'].append(params[1])
        centers['stdev'].append(np.sqrt(params_std[1][1]))
        scales['est'].append(params[2])
        scales['stdev'].append(np.sqrt(params_std[2][2]))

    return amplitudes, centers, scales


def plot_values(index_data, parameteres, params_name):
    """
    Plots values (parameteres) by index.
    """
    est_list = np.array(parameteres['est'])
    stdev_list = np.array(parameteres['stdev'])
    mask = (np.abs(stdev_list / est_list) < 0.25)
    # np: masked array

    plt.grid()
    plt.xlabel('index')
    plt.ylabel(params_name)
    plt.errorbar(index_data[mask], est_list[mask], yerr=stdev_list[mask], fmt="ro", markersize=2)
    plt.savefig(params_name + '.pdf')
    plt.clf()


def plot_slices(data, amplitudes, centers, scales, directory):
    """
    Plots slice data and approximation curves. Creates directory if it is needed.
    """
    if not os.path.exists(directory):
        os.makedirs(directory)

    x_data = np.arange(len(data[0,:]))
    for i in range(len(data[:,0])):
        f = lambda x: model_fun(x, amplitudes['est'][i], centers['est'][i], scales['est'][i])
        x = np.linspace(x_data[0], x_data[-1], 1000)

        plt.grid()
        plt.xlabel('index')
        plt.ylabel('flux')
        plt.plot(x_data, data[i,:], 'ro', label="slice data")
        plt.plot(x, f(x), label="fit curve")
        plt.legend(loc='upper left')
        plt.savefig(directory + '/' + str(i) + '.pdf')
        plt.clf()


def process_galaxy(data):
    """
    Fits and plots slice datas from file_path fits file.
    """
    amplitudes, centers, scales = get_slice_parameters(data)

    index_data = np.arange(len(data[:,0]))
    plot_values(index_data, amplitudes, 'amplitudes')
    plot_values(index_data, centers, 'centers')
    plot_values(index_data, scales, 'scales')

    plot_slices(data, amplitudes, centers, scales, 'slice_plts')


def main(args):
    # glob: find by mask
    # path lib
    # https://docs.python.org/3/library/functools.html#functools.partial
    fits_file_path_list = []
    for my_file in os.listdir(args.data_dir):
        if my_file.endswith(".fits"):
            fits_file_path_list.append(os.path.join(args.data_dir, my_file))

    result_directory = 'result_data'
    if not os.path.exists(result_directory):
        os.makedirs(result_directory)
    os.chdir(result_directory)

    for fits_file_path in fits_file_path_list:
        directory = os.path.splitext(os.path.basename(fits_file_path))[0]
        print("Fitting and plotting data " + directory)
        if not os.path.exists(directory):
            os.makedirs(directory)
        os.chdir(directory)

        data = fits.getdata('../../' + fits_file_path)
        process_galaxy(data)
        os.chdir('..')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('data_dir', action='store', type=str, help='input name of data directory')
    args = parser.parse_args()
    main(args)
